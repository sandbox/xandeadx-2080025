<?php

/**
 * Implements hook_views_data().
 */
function views_created_or_commented_views_data() {
  $data['node_comment_statistics']['created_or_commented'] = array(
    'title' => t('Created/commented date'),
    'help' => t('The most recent of last comment posted or node created time.'),
    'field' => array(
      'handler' => 'views_handler_field_created_or_commented',
      'click sortable' => TRUE,
      'no group by' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort_created_or_commented',
      'no group by' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_created_or_commented',
    ),
  );
  
  return $data;
}
